package com.example.audioplaying.api.model

data class AudioResponseModel(
    val id: Int,
    val title: String,
    val artist: String,
    val play_time: Int,
    val url: String
)