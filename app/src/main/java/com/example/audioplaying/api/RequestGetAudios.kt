package com.example.audioplaying.api

data class RequestGetAudios(val token: String)