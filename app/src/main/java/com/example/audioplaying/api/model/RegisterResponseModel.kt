package com.example.audioplaying.api.model

data class RegisterResponseModel(val token: String)
