package com.example.audioplaying.api

import com.example.audioplaying.api.model.AudioResponseModel
import com.example.audioplaying.api.model.RegisterResponseModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface AudioService {
    @POST("register")
    suspend fun register(
        @Body request: RegistrationRequestModel
    ): Response<RegisterResponseModel>

    @GET("get-tracks")
    suspend fun getAudios(
        @Query("token") apiToken: String
    ): Response<List<AudioResponseModel>>
}