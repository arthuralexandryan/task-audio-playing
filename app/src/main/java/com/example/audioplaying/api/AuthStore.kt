package com.example.audioplaying.api

import android.content.Context
import android.preference.PreferenceManager
import com.example.audioplaying.util.pref.StringPreference
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class AuthStore @Inject constructor(@ApplicationContext private val context: Context) {
    private val sharePref by lazy { PreferenceManager.getDefaultSharedPreferences(context) }
    private val tokenPref by lazy { StringPreference(sharePref, REGISTRATION) }


	companion object {
		const val REGISTRATION = "registration"
	}

	fun saveToken(token: String? = "") {
		tokenPref.save(token)
	}

	fun getToken(): String? {
		return tokenPref.get()
	}
}