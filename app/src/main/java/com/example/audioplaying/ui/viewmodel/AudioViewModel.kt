package com.example.audioplaying.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.audioplaying.api.model.AudioResponseModel
import com.example.audioplaying.repository.AudioRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AudioViewModel @Inject constructor(
    private val repo: AudioRepositoryImpl
): ViewModel() {
    fun getAudios(token: String): StateFlow<List<AudioResponseModel>?> {
        val data = MutableStateFlow<List<AudioResponseModel>?>(emptyList())
        viewModelScope.launch {
            repo.getAudios(token).collect { response ->
                if (response.isSuccessful){
                    data.value = response.body()
                }
            }
        }
        return data
    }
}