package com.example.audioplaying.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.audioplaying.api.model.RegisterResponseModel
import com.example.audioplaying.repository.AudioRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class RegistrationViewModel @Inject constructor(
    private val repo: AudioRepositoryImpl
) : ViewModel() {
    fun registration(uuid: String): StateFlow<Response<RegisterResponseModel>?> {
        val data = MutableStateFlow<Response<RegisterResponseModel>?>(null)
        viewModelScope.launch {
            repo.register(uuid = uuid).collect { response ->
                data.value = response
            }
        }
        return data
    }
}