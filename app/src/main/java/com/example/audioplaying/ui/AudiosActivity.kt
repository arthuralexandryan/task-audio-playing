package com.example.audioplaying.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.audioplaying.ui.viewmodel.AudioViewModel
import com.example.audioplaying.api.AuthStore
import com.example.audioplaying.adapter.AudioAdapter
import com.example.audioplaying.databinding.ActivityAudioBinding
import com.example.audioplaying.util.initChipRecycle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

@AndroidEntryPoint
class AudiosActivity : AppCompatActivity() {
    private val viewModel: AudioViewModel by viewModels()
    private lateinit var binding: ActivityAudioBinding
    @Inject
    lateinit var audioAdapter: AudioAdapter
    @Inject
    lateinit var authStore: AuthStore
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityAudioBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initRecyclerView()
        initListAudio()
    }

    private fun initListAudio() {
        authStore.getToken()?.run {
            lifecycleScope.launchWhenResumed {
                viewModel.getAudios(this@run).collectLatest { audioList ->
                    audioList?.run {
                        audioAdapter.listAudios = this
                        audioAdapter.notifyDataSetChanged()
                    }
                }
            }

        }
    }

    private fun initRecyclerView() {
        binding.rvAudios.initChipRecycle(audioAdapter)
    }
}