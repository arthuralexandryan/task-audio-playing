package com.example.audioplaying.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.lifecycle.lifecycleScope
import com.example.audioplaying.api.AuthStore
import com.example.audioplaying.R
import com.example.audioplaying.ui.viewmodel.RegistrationViewModel
import com.example.audioplaying.api.model.RegisterResponseModel
import com.example.audioplaying.util.UUID
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import retrofit2.Response
import javax.inject.Inject

@AndroidEntryPoint
class RegistrationActivity : AppCompatActivity() {

    private val viewModel: RegistrationViewModel by viewModels()

    @Inject
    lateinit var authStore: AuthStore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        authStore = AuthStore(this)

        setContentView(R.layout.activity_registration)

        checkRegistration()
        initRegisterListener()
    }

    private fun checkRegistration() {
        if (authStore.getToken() != null){
            startActivity(Intent(this@RegistrationActivity, AudiosActivity::class.java))
            finish()
        }
    }

    private fun initRegisterListener() {
        findViewById<AppCompatButton>(R.id.register).setOnClickListener {
            registration()
        }
    }

    private fun registration() {
        lifecycleScope.launchWhenResumed {
            viewModel.registration(uuid = UUID).collectLatest { response ->
                handleResponse(response)
            }
        }
    }

    private fun handleResponse(response: Response<RegisterResponseModel>?) {
        response?.run {
            if (isSuccessful) {
                authStore.saveToken(body()?.token)
                startActivity(Intent(this@RegistrationActivity, AudiosActivity::class.java))
            } else {
                Toast.makeText(this@RegistrationActivity, "Something went wrong", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }
}