package com.example.audioplaying.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.audioplaying.api.model.AudioResponseModel
import com.example.audioplaying.player.AudioView
import com.example.audioplaying.player.MediaPlayerHelper
import javax.inject.Inject

class AudioAdapter @Inject constructor(): RecyclerView.Adapter<AudioViewHolder>() {
     var listAudios: List<AudioResponseModel> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AudioViewHolder  =
        AudioViewHolder(AudioView(parent.context))

    override fun onBindViewHolder(holder: AudioViewHolder, position: Int) {
        holder.onBind(listAudios[position])
    }

    override fun onViewDetachedFromWindow(holder: AudioViewHolder) {
        MediaPlayerHelper.pauseAudiosOnWindowFocus(false)
    }

    override fun getItemCount(): Int = listAudios.size
}