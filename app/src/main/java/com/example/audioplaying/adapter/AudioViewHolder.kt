package com.example.audioplaying.adapter

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import com.example.audioplaying.api.model.AudioResponseModel
import com.example.audioplaying.player.AudioView
import com.example.audioplaying.player.MediaPlayerHelper.audioArray
import javax.inject.Inject

class AudioViewHolder @Inject constructor(private val view: AudioView): RecyclerView.ViewHolder(view) {

    lateinit var onPlay: (Int) -> Unit

    @SuppressLint("SetTextI18n")
    fun onBind(audioModel: AudioResponseModel) {
        view.url = audioModel.url
        view.duration = audioModel.play_time.toFloat()
        view.audioId = audioModel.id
        audioArray.add(view)
        view.artists.text = "${audioModel.artist} / ${audioModel.title}"
    }
}