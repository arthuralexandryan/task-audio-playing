package com.example.audioplaying.repository

import com.example.audioplaying.api.AudioService
import com.example.audioplaying.api.RegistrationRequestModel
import com.example.audioplaying.api.model.AudioResponseModel
import com.example.audioplaying.api.model.RegisterResponseModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import retrofit2.Response
import javax.inject.Inject

class AudioRepositoryImpl @Inject constructor(
    private val service: AudioService
): AudioRepository {
    override suspend fun register(uuid: String): Flow<Response<RegisterResponseModel>> =
        flowOf(service.register(RegistrationRequestModel(uuid = uuid)))

    override suspend fun getAudios(token: String): Flow<Response<List<AudioResponseModel>>> =
        flowOf(service.getAudios(token))

}
