package com.example.audioplaying.repository

import com.example.audioplaying.api.model.AudioResponseModel
import com.example.audioplaying.api.model.RegisterResponseModel
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

interface AudioRepository {
    suspend fun register(
        uuid: String
    ): Flow<Response<RegisterResponseModel>>

    suspend fun getAudios(
        token: String
    ): Flow<Response<List<AudioResponseModel>>>
}