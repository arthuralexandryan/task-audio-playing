package com.example.audioplaying.player

import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.collection.ArraySet
import com.example.audioplaying.R

object MediaPlayerHelper {
	
	val audioArray = ArraySet<AudioView>()

	var currentAudioId: Int = 0
	
	fun checkAudioId(audioId: Int): Boolean = currentAudioId == audioId
	
	fun stoppedAudios(audioId: Int, forNavigate: Boolean) {
		audioArray.forEach { audioView ->
			if (forNavigate || audioView.audioId != audioId) {
				audioView.updatePlayerStatus?.onUpdate(false)
				audioView.onRemove()
				defaultSeekBar(audioView)
			}
		}
		
		if (forNavigate) currentAudioId = 0
	}
	
	fun pauseAudios(audioId: Int) {
		audioArray.forEach { audioView ->
			if (audioView.audioId != audioId) {
				audioView.onPause()
			}
		}
	}
	
	fun pauseAudiosOnWindowFocus(hasFocus: Boolean) {
		if (!hasFocus) {
			audioArray.forEach { audioView ->
				if (audioView.isPlaying()) {
					currentAudioId = audioView.audioId
				}
				audioView.onPause()
			}
		}
	}
	
	fun defaultSeekBar(audioView: AudioView) {
		audioView.run {
			seekBarProgress.progress = 0
			passedTime.text = getDurationBreakdown(0)
			fullTime.text = (
				"-${duration
					.toLong()
					.let { getDurationBreakdown(it) }}"
			                )
			playPause.setImageResource(R.drawable.ic_play_24)
			shouldPlayAfterSeek = false
			
			playPause.isEnabled = true
			seekBarProgress.isEnabled = true
			seekPosition = -1F
		}
	}
	
	fun pauseSeekBar(audioView: AudioView) {
		audioView.run {
			seekBarProgress.isEnabled = false
			passedTime.text = getDurationBreakdown(seekBarProgress.progress.toLong())
			fullTime.text = audioView.playerAudio?.getDuration()
				?.toLong()
				?.let { getDurationBreakdown(it) }
			playPause.setImageResource(R.drawable.ic_play_24)
		}
	}
	
	fun getDurationToText(time: String?) = time?.let {
		val timeInMilliSec = (time.toDouble() * 1000).toLong()
		val duration = timeInMilliSec / 1000
		val hours = duration / 3600
		val minutes = (duration - hours * 3600) / 60
		val seconds = duration - (hours * 3600 + minutes * 60)
		"-$minutes:${if (seconds > 9) "$seconds" else "0${seconds}"}"
	}.toString()
	
	fun getAudioDuration(url: String?) = url?.let {
		val retriever = MediaMetadataRetriever()
		retriever.setDataSource(url, HashMap<String, String>())
		retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
	}
}

fun isNetworkConnectedCheckByContext(context: Context): Boolean {
	return try {
		val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			val nw = connectivityManager.activeNetwork ?: return false
			val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
			return when {
				actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
				actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
				actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
				else -> false
			}
		}
		else {
			return connectivityManager.isDefaultNetworkActive
		}
	}
	catch (e: NullPointerException) {
		false
	}
}