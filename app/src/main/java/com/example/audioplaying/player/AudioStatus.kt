package com.example.audioplaying.player

enum class AudioStatus(val status: String) {
	ZERO("0"),
	START("1"),
	END("2")
}

enum class PlayingState(val state: String){
	START("start"),
	END("end"),
	PAUSED("paused")
}