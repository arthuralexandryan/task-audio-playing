package com.example.audioplaying.player

interface OnUpdatePlayerStatus {
	fun onUpdate(isPlaying: Boolean)
	fun onPlay()
	fun onEnd()
}