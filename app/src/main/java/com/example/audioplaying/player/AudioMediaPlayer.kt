package com.example.audioplaying.player

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioFocusRequest
import android.media.AudioManager
import android.media.AudioManager.OnAudioFocusChangeListener
import android.media.MediaPlayer
import android.media.MediaPlayer.*
import android.os.Build
import android.os.Handler
import androidx.annotation.RequiresApi


@RequiresApi(Build.VERSION_CODES.O)
class AudioMediaPlayer(
	context: Context,
	private val url: String,
	private val playingProgress: PlayingProgress,
	private val progressBarMax: Int
) : OnCompletionListener,
	OnPreparedListener, OnErrorListener, OnSeekCompleteListener,
	OnInfoListener, OnBufferingUpdateListener, OnAudioFocusChangeListener {
	
	private var mediaPlayer: MediaPlayer? = null
	private var lengthInMillis: Float = 0F
	private var audioLength: Float = 0F
	private var resumePosition: Float = 0F
	
	private val audioManager: AudioManager by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
		context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
	}
	
	
	private val audioFocusRequest: AudioFocusRequest.Builder by lazy {
		AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
			.setAudioAttributes(
				AudioAttributes.Builder()
					.setUsage(AudioAttributes.USAGE_GAME)
					.setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
					.build()
			)
		
	}
	private val seekBarUpdateHandler: Handler = Handler()
	private lateinit var updateSeekBar: Runnable
	
	init {
		updateSeekBar = Runnable {
			mediaPlayer?.run {
				if (isPlaying) {
					playingProgress.onUpdateTrack(
						currentPosition, ((currentPosition / 1000) * lengthInMillis).toInt(), this.duration
					)
					seekBarUpdateHandler.postDelayed(updateSeekBar, 500)
				}
			}
		}
		requestAudioFocus()
		if (url != "") initMediaPlayer()
	}
	
	private fun initMediaPlayer() {
		mediaPlayer = MediaPlayer()
		mediaPlayer?.run {
			setOnCompletionListener(this@AudioMediaPlayer)
			setOnErrorListener(this@AudioMediaPlayer)
			setOnPreparedListener(this@AudioMediaPlayer)
			setOnBufferingUpdateListener(this@AudioMediaPlayer)
			setOnSeekCompleteListener(this@AudioMediaPlayer)
			setOnInfoListener(this@AudioMediaPlayer)
			reset()
			setAudioAttributes(
				AudioAttributes
					.Builder()
					.setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
					.build()
			)
			
			if (runCatching { setDataSource(url) }.isSuccess) {
				prepareAsync()
			}
			else {
				mediaPlayer = null
			}
			
		}
		
	}
	
	private fun requestAudioFocus(): Boolean {
		return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			audioManager.requestAudioFocus(
				audioFocusRequest.setAcceptsDelayedFocusGain(true)
					.setOnAudioFocusChangeListener(this)
					.build()
			) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED
		}
		else {
			audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED
		}
	}
	
	override fun onCompletion(mp: MediaPlayer?) {
		stopMedia()
		playingProgress.onFinishTrack()
		resumePosition = 0F
	}
	
	override fun onPrepared(mp: MediaPlayer?) {
		audioLength = mp?.duration?.toFloat() ?: 0F
		playingProgress.onMediaPlayerPrepared(mp?.duration?.toLong())
		lengthInMillis = progressBarMax / (audioLength / 1000)
	}
	
	override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
		playingProgress.onError(mp, what, extra)
		return false
	}
	
	override fun onInfo(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
		return false
	}
	
	override fun onBufferingUpdate(mp: MediaPlayer?, percent: Int) {
		playingProgress.onBufferingUpdate(percent)
	}
	
	override fun onAudioFocusChange(focusChange: Int) {
		changeFocus(focusChange)
	}
	
	private fun changeFocus(focusChange: Int) {
		when (focusChange) {
			AudioManager.AUDIOFOCUS_GAIN -> {
				mediaPlayer.apply {
					when {
						this == null -> initMediaPlayer()
						else -> {/*if (!this.isPlaying) this.start()*/}
					}
				}?.setVolume(1.0F, 1.0F)
				
				
			}
			AudioManager.AUDIOFOCUS_LOSS -> {
				mediaPlayer?.run {
				}
			}
			AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> mediaPlayer?.run { if (isPlaying) pause() }
			AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> mediaPlayer?.run { if (isPlaying) setVolume(0.1F, 0.1F) }
			
		}
	}
	
	override fun onSeekComplete(mp: MediaPlayer?) {}
	
	fun onRemove() {
		mediaPlayer?.run {
			seekBarUpdateHandler.removeCallbacks(updateSeekBar)
			stopMedia()
			release()
		}
		removeAudioFocus()
	}
	
	private fun removeAudioFocus() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			audioManager.abandonAudioFocusRequest(audioFocusRequest.build())
		}
		else {
			audioManager.abandonAudioFocus(this)
		}
	}
	
	fun getDuration(): Int = mediaPlayer?.duration ?: 0
	
	fun getCurrentPosition(): Int = mediaPlayer?.currentPosition ?: 0
	
	fun isPlaying(): Boolean = mediaPlayer?.isPlaying ?: false
	
	fun playMedia(x: Float) {
		mediaPlayer?.run {
			if (!isPlaying) {
				seekTo(x.toInt())
				start()
				seekBarUpdateHandler.postDelayed(updateSeekBar, 0)
			}
			return
		}
		initMediaPlayer()
		playMedia(0F)
	}
	
	fun resumeMedia() {
		mediaPlayer?.run {
			if (!isPlaying) {
				seekTo(resumePosition.toInt())
				start()
				seekBarUpdateHandler.postDelayed(updateSeekBar, 0)
			}
		}
	}
	
	fun pauseMedia() {
		mediaPlayer?.run {
			if (isPlaying) {
				seekBarUpdateHandler.removeCallbacks(updateSeekBar)
				pause()
				resumePosition = currentPosition.toFloat()
			}
		}
	}
	
	private fun stopMedia() {
		mediaPlayer?.run {
			if (isPlaying) {
				resumePosition = 0F
				stop()
			}
		}
	}
	
	fun getResumePosition(): Float = resumePosition
	
	
	fun setResumePosition(rPosition: Float) {
		this.resumePosition = (audioLength * rPosition) / 100
	}
	
	fun setResumePosition(rPosition: Float, duration: Float) {
		this.resumePosition = (duration * rPosition) / 100
	}
	
	fun hasProgress(): Boolean = (if (mediaPlayer != null) mediaPlayer!!.currentPosition else 0) > 0.0 || resumePosition > 0.0
}
