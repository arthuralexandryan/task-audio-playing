package com.example.audioplaying.player

import android.media.MediaPlayer

interface PlayingProgress {
	fun onBufferingUpdate(percent: Int)
	fun onMediaPlayerPrepared(audioLength: Long?)
	fun onUpdateTrack(progressSeconds: Int, progressMillis: Int, duration: Int)
	fun onFinishTrack()
	fun onError(mp: MediaPlayer?, what: Int, extra: Int)
	fun onFocusLoss(mediaPlayer: MediaPlayer)
}