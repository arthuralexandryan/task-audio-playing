package com.example.audioplaying.player

import android.content.Context
import android.media.MediaPlayer
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatSeekBar
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import com.example.audioplaying.R
import com.example.audioplaying.player.MediaPlayerHelper.checkAudioId
import com.example.audioplaying.player.MediaPlayerHelper.currentAudioId
import com.example.audioplaying.player.MediaPlayerHelper.defaultSeekBar
import com.example.audioplaying.player.MediaPlayerHelper.pauseAudios
import java.util.concurrent.TimeUnit

class AudioView : LinearLayoutCompat {
    var audioId: Int = 0
    var url: String = ""
    var playerAudio: AudioMediaPlayer? = null
    var shouldPlayAfterSeek: Boolean = false
    private var serviceBound: Boolean = false
    var updatePlayerStatus: OnUpdatePlayerStatus? = null
    var seekPosition: Float = -1F
    var duration: Float = 0F

    val playPause: AppCompatImageView by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        findViewById(R.id.play_pause)
    }

    val passedTime: AppCompatTextView by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        findViewById(R.id.seekBarPassed)
    }

    val fullTime: AppCompatTextView by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        findViewById(R.id.seekBarFull)
    }

    val seekBarProgress: AppCompatSeekBar by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        findViewById(R.id.seekBar_progress)
    }

    val loading: ProgressBar by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        findViewById(R.id.loading)
    }

    val artists: AppCompatTextView by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        findViewById(R.id.txt_artist)
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributes: AttributeSet?) : this(context, attributes, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initViews(context)
    }

    fun setDuration(duration: String?) {
        duration?.let {
            this.duration = it.toFloat() * 1000
        }
    }

    private val playingProgress: PlayingProgress = object : PlayingProgress {
        override fun onBufferingUpdate(percent: Int) {}

        override fun onMediaPlayerPrepared(audioLength: Long?) {
            passedTime.text = getDurationBreakdown(0)
            fullTime.text = audioLength?.let { getDurationBreakdown(it) }
            playPause.isEnabled = true
            seekBarProgress.isEnabled = true
            playPause.visibility = View.VISIBLE
            loading.visibility = View.INVISIBLE
            if (!checkAudioId(audioId)) {
                currentAudioId = audioId
                playPause.setImageResource(R.drawable.ic_pause_24)
                playerAudio?.playMedia((duration * seekPosition) / 100)
                pauseAudios(audioId)
                updatePlayerStatus?.run {
                    onUpdate(true)
                    onPlay()
                }
            }
        }

        override fun onUpdateTrack(progressSeconds: Int, progressMillis: Int, duration: Int) {
            seekBarProgress.progress = progressMillis
            passedTime.text = getDurationBreakdown(progressSeconds.toLong())
            fullTime.text = (
                    "-${
                        duration
                            .toLong()
                            .minus(progressSeconds.toLong())
                            .let { getDurationBreakdown(it) }
                    }"
                    )
        }

        override fun onFinishTrack() {
            defaultSeekBar(this@AudioView)
            updatePlayerStatus?.run {
                onUpdate(false)
                onEnd()
            }
        }

        override fun onError(mp: MediaPlayer?, what: Int, extra: Int) {
            var error = ""
            when (what) {
                MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK ->
                    error = context.getString(R.string.error_audioError_description)
                MediaPlayer.MEDIA_ERROR_SERVER_DIED ->
                    error = context.getString(R.string.error_audioError_description)
                MediaPlayer.MEDIA_ERROR_UNKNOWN ->
                    error = context.getString(R.string.error_audioError_description)
                MediaPlayer.MEDIA_ERROR_IO ->
                    error = context.getString(R.string.error_audioError_description)
                MediaPlayer.MEDIA_ERROR_MALFORMED ->
                    error = context.getString(R.string.error_audioError_description)
                MediaPlayer.MEDIA_ERROR_UNSUPPORTED ->
                    error = context.getString(R.string.error_audioError_description)
                MediaPlayer.MEDIA_ERROR_TIMED_OUT ->
                    error = context.getString(R.string.error_audioError_description)
            }
            playPause.isEnabled = true
            seekBarProgress.isEnabled = true
        }

        override fun onFocusLoss(mediaPlayer: MediaPlayer) {
        }
    }

    private fun initViews(context: Context) {
        orientation = HORIZONTAL
        gravity = Gravity.CENTER
        LayoutInflater.from(context).inflate(R.layout.audio_layout, this, true)
        playPause.setOnClickListener {
            if (checkAudioId(audioId)) {
                playerAudio?.run {
                    play(this)
                }
            } else {
                if ((playerAudio != null && playerAudio!!.isPlaying()) || isNetworkConnectedCheckByContext(
                        this.context
                    )
                ) {
                    pauseAudios(audioId)
                    if (playerAudio == null) {
                        serviceBound = false
                        playPause.isEnabled = false
                        seekBarProgress.isEnabled = false
                        initAudio(context)
                    } else {
                        // else for pause state
                        playerAudio?.run {
                            play(this)
                        }
                    }
                }
            }
        }

        seekBarProgress.progress = 0
        seekBarProgress.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                playerAudio?.run {
                    passedTime.text = getDurationBreakdown(
                        getCurrentPosition().toLong()
                    )
                    setResumePosition(seekBar.progress.toFloat())
                    passedTime.text = getDurationBreakdown(getResumePosition().toLong())
                    fullTime.text = (
                            "-${
                                duration
                                    .toLong()
                                    .minus(getResumePosition().toLong())
                                    .let { getDurationBreakdown(it) }
                            }"
                            )
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                playerAudio?.run {
                    if (isPlaying()) {
                        pauseMedia()
                        playPause.setImageResource(R.drawable.ic_play_24)
                        updatePlayerStatus?.run {
                            onUpdate(false)
                        }
                    }
                }
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                playerAudio?.run {
                    setResumePosition(seekBar.progress.toFloat())
                    if (shouldPlayAfterSeek) {
                        resumeMedia()
                        playPause.setImageResource(R.drawable.ic_pause_24)
                        updatePlayerStatus?.run {
                            onUpdate(true)
                        }
                    } else {
                        passedTime.text = getDurationBreakdown(
                            getResumePosition().toLong()
                        )
                    }
                    return
                }
                seekPosition = seekBar.progress.toFloat()
            }
        })
    }

    fun isPlaying(): Boolean {
        playerAudio?.run {
            return isPlaying()
        }
        return false
    }

    private fun play(playerAudio: AudioMediaPlayer) {
        if (playerAudio.isPlaying()) {
            currentAudioId = audioId
            playerAudio.pauseMedia()
            playPause.setImageResource(R.drawable.ic_play_24)
            shouldPlayAfterSeek = false
            updatePlayerStatus?.run {
                onUpdate(false)
            }
        } else {

            if (playerAudio.hasProgress()) {
                playerAudio.resumeMedia()
                updatePlayerStatus?.run {
                    onUpdate(true)
                }
                pauseAudios(audioId)
            } else {
                playerAudio.playMedia(0F)
            }
            playPause.setImageResource(R.drawable.ic_pause_24)
            shouldPlayAfterSeek = true
        }
    }

    private fun initAudio(context: Context) {
        if (!serviceBound) {
            playPause.visibility = View.INVISIBLE
            loading.visibility = View.VISIBLE
            playerAudio = if (seekPosition < 0) {
                AudioMediaPlayer(context, url, playingProgress, seekBarProgress.max)
            } else {
                AudioMediaPlayer(context, url, playingProgress, seekBarProgress.max)
            }
            serviceBound = true
        }
    }

    fun onPause() {
        playerAudio?.run {
            playPause.setImageResource(R.drawable.ic_play_24)
            pauseMedia()
            shouldPlayAfterSeek = false
            updatePlayerStatus?.run {
                onUpdate(false)
            }
        }
    }

    fun onRemove() {
        playerAudio?.run {
            onRemove()
            loading.visibility = View.INVISIBLE
            playPause.visibility = View.VISIBLE
            playPause.setImageResource(R.drawable.ic_play_24)
        }
        playerAudio = null
    }
}

fun getDurationBreakdown(millis: Long): String {
    if (millis < 0) return "00"

    var mil: Long = millis
    val days: Long = TimeUnit.MILLISECONDS.toDays(mil)
    mil -= TimeUnit.DAYS.toMillis(days)
    val hours: Long = TimeUnit.MILLISECONDS.toHours(mil)
    mil -= TimeUnit.HOURS.toMillis(hours)
    val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(mil)
    mil -= TimeUnit.MINUTES.toMillis(minutes)
    val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(mil)

    val stringBuilder = StringBuilder(64)
    var isMin = false

    if (days > 0) {
        stringBuilder.append(days)
        stringBuilder.append("00")
    }
    if (hours > 0) {
        stringBuilder.append(if (hours > 9) "$hours:" else "0$hours:")
    }
    if (minutes > 0) {
        // if minutes is 0 shows "00:" , if minutes small than 10 shows like "05:"
        stringBuilder.append(if (minutes == 0L) "00:" else if (minutes > 9) minutes.toString() else "0$minutes:")
        isMin = true
    }
    if (seconds > 0) {
        // if seconds is 0 shows "00:" , if seconds small than 10 seconds like "05"
        stringBuilder.append(
            if (isMin) {
                when {
                    seconds == 0L -> "00"
                    seconds > 9 -> "$seconds"
                    else -> "0$seconds"
                }
            } else
                when {
                    seconds == 0L -> "00"
                    seconds > 9 -> "0:$seconds"
                    else -> "0:0$seconds"
                }
        )
    }
    return if (stringBuilder.toString() == "") "0:00" else stringBuilder.toString()
}
