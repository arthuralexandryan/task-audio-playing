package com.example.audioplaying.util

import androidx.room.TypeConverter
import com.example.audioplaying.api.model.AudioResponseModel
import com.google.gson.Gson

class TopicsConverter {
    @TypeConverter
    fun listToJson(value: List<AudioResponseModel>?): String = Gson().toJson(value)

    @TypeConverter
    fun jsonToList(value: String?) = Gson().fromJson(value, Array<AudioResponseModel>::class.java).toMutableList()
}