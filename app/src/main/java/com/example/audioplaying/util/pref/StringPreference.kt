package com.example.audioplaying.util.pref

import android.content.SharedPreferences
import androidx.core.content.edit

class StringPreference(
	private val sharedPreferences: SharedPreferences,
	val key: String
) {
	
	fun get(): String? {
		return sharedPreferences.getString(key, null)
	}
	
	fun save(value: String?) {
		sharedPreferences.edit { putString(key, value) }
	}
}