package com.example.audioplaying.util

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun <Adapter>RecyclerView.initChipRecycle(
    appAdapter: Adapter,
    isVertical: Boolean = true,
    isSizeFixed: Boolean = true
) {
    layoutManager = LinearLayoutManager(
        context,
        if (isVertical) LinearLayoutManager.VERTICAL else LinearLayoutManager.HORIZONTAL,
        false
    )
    adapter = appAdapter as RecyclerView.Adapter<*>
    isNestedScrollingEnabled = false
    setHasFixedSize(isSizeFixed)
}